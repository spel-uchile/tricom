import serial
import struct
import time
import argparse
import datetime

parser = argparse.ArgumentParser()
parser.add_argument("port", metavar='PORT', help="File with access list")
parser.add_argument("data", metavar='DATA', help="Task file")
parser.add_argument("-d", "--delay", default=10, type=float, help="Delay between messages")
parser.add_argument("-n", "--nmax", default=60, type=int, help="Number of messages")

args = parser.parse_args()

ser=serial.Serial(port=args.port,
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
    )

print("Console open: {}".format(ser.isOpen()))

print("Sending setup commands")
cmds = [b'b 1\r\n', b'a 3\r\n', b'z\r\n']
for cmd in cmds:
    print(cmd)
    ser.write(cmd)
    ans = ser.read(11)
    print(' ', ans)

print("Sending data...")
log = []
user_data = args.data if len(args.data) == 4 else args.data[:4]
try:
    for i in range(args.nmax):
        ts = time.time()
        data = b'\x08\x38' + bytes("{:02d}".format(i%100), 'ascii') + bytes(user_data, 'ascii')
        log.append("{}, {}, {}\n".format(datetime.datetime.now(), i, data[2:]))
        print(log[-1])
        ser.write(data)
        ans = ser.read(4)
        log.append("{}\n".format(ans))
        print(log[-1])
        delay = args.delay - (time.time() - ts)
        time.sleep(delay if delay > 0 else 0)
except TypeError as e:
    print(e)
except KeyboardInterrupt as e:
    print(e)
except Exception as e:
    print(e)
finally:
    print("Closing!")
    ser.close()
    with open("{}.txt".format(time.time()), "w") as logfile:
        logfile.writelines(log)

