import serial
import struct
import time
import argparse
import datetime
import threading

CRCPOLY1 = 0x1021
CHAR_BIT =8


class CmdDummyFrame(object):
    def __init__(self):
        self.stx_16 = 0xEB90  # H
        self.len_8 = 11  # cmd dummy len B
        self.fp1_8 = 0  # B
        self.fp2_8 = 0xFF  # B
        self.fp3_8 = 0x02  # B
        self.fp4_8 = 0  # B
        self.obctime_64 = 0  # L
        self.direct_8 = 0  # B
        self.cmdcount_8 = 0  # B
        self.cmdid_8 = 0xA0  # cmd dummy id B
        self.params = None  # s
        self.crc_16 = 0  # H
        self.etx_16 = 0xC579  # H

        self._header_str = "!HB"
        self._fp_str = "!BBBBLBBB"
        self._params_str = None
        self._footer_str = "!HH"

    def __str__(self):
        return ' '.join('{:02x}'.format(x) for x in self.to_bytes())

    def set_data(self, data: bytes, fmt: str = "s"):
        self.params = data
        self._params_str = fmt

    def to_bytes(self):
        """
        Translate struct to byte array
        :return: Byte array

        >>> cmd_dummy_ok = b'\\xEB\\x90\\x0B\\x00\\xFF\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xA0\\x1E\\x89\\xC5\\x79'
        >>> cmd_dummy = CmdDummyFrame().to_bytes()
        >>> cmd_dummy == cmd_dummy_ok
        True
        """

        # Fixed parameters to bytes
        bytes_fp = struct.pack(self._fp_str, self.fp1_8, self.fp2_8, self.fp3_8, self.fp4_8, self.obctime_64, self.direct_8, self.cmdcount_8, self.cmdid_8)

        # Data to bytes (if available)
        bytes_data = b''
        if self.params and self._params_str:
            bytes_data += struct.pack(self._params_str, *self.params)

        # Fixed parameters + Data CRC
        self.crc_16 = crc(bytes_fp+bytes_data)
        # print(hex(self.crc_16))
        # Fixed parameters + Data lenght
        self.len_8 = len(bytes_fp+bytes_data)

        # Get header and footr
        bytes_hdr = struct.pack(self._header_str, self.stx_16, self.len_8)
        bytes_ftr = struct.pack(self._footer_str, self.crc_16, self.etx_16)

        # Build full packets
        bytes_all = bytes_hdr + bytes_fp + bytes_data + bytes_ftr
        return bytes_all


class CmdAliveFrame(CmdDummyFrame):
    def __init__(self):
        """
        >>> cmd_alive_ok = b'\\xEB\\x90\\x0B\\x00\\xFF\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x01\\xA0\\x2D\\xB8\\xC5\\x79'
        >>> cmd_alive = CmdAliveFrame().to_bytes()
        >>> cmd_alive == cmd_alive_ok
        True
        """
        super(CmdAliveFrame, self).__init__()
        self.cmdid_8 = 0xA0  # SF_ALIVE
        self.cmdcount_8 = 0x01  # SF_ALIVE


class CmdTransferFrame(CmdDummyFrame):
    def __init__(self):
        super(CmdTransferFrame, self).__init__()
        self.cmdcount_8 = 0x08
        self.cmdid_8 = 0xA4
        self.params = []
        self._params_str = "BBB"

    def set_params(self, sector, address, n):
        """
        Set WM_TRANSFER command's parameters
        :param sector: Flash memory sector= 0 - 16
        :param address: Flash memory inner address =0 -255
        :param n:Number to be transferred
        :return: None

        >>> cmd_trans_ok = b'\\xEB\\x90\\x0E\\x00\\xFF\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x08\\xA4\\x00\\x01\\x02\\x22\\xCD\\xC5\\x79'
        >>> cmd_trans = CmdTransferFrame()
        >>> cmd_trans.set_params(0, 1, 2)
        >>> cmd_trans_bytes = cmd_trans.to_bytes()
        >>> cmd_trans_bytes == cmd_trans_ok
        True
        """
        self.params = [sector, address, n]


def crc(command: bytes) -> int:
    """
    :param command: Calculate bytes CRC
    :return: CRC

    >>> data = b'\\x00\\xFF\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x08\\xA4\\x00\\x01\\x02'
    >>> crc_data = crc(data)
    >>> print(hex(crc_data))
    0x22cd
    """
    r = 0x0000
    mask = 0xFFFF

    for c in command:
        r ^= (c << 16-CHAR_BIT) & mask
        for j in range(CHAR_BIT):
            if r & 0x8000:
                r = ((r << 1) & mask) ^ CRCPOLY1
            else:
                r = (r << 1) & mask
    return r


def cmd_crc(cmd):
    """
    Append command CRC. Replace CRC bytes in cmd tail.
    :param cmd: Full command as byte array. Include header, data, and tail.
    :return: Cmd byte array with modified CRC field

    >>> dummy = b'\\xEB\\x90\\x0B\\x00\\xFF\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xA0\\x00\\x00\\xC5\\x79'
    >>> dummy_ok = b'\\xEB\\x90\\x0B\\x00\\xFF\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xA0\\x1E\\x89\\xC5\\x79'
    >>> dummy = cmd_crc(dummy)
    >>> str(dummy) == str(dummy_ok)
    True
    >>> alive = b'\\xEB\\x90\\x0B\\x00\\xFF\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x01\\xA0\\x00\\x00\\xC5\\x79'
    >>> alive_ok = b'\\xEB\\x90\\x0B\\x00\\xFF\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x01\\xA0\\x2D\\xB8\\xC5\\x79'
    >>> alive = cmd_crc(alive)
    >>> str(alive) == str(alive_ok)
    True
    """
    cmd = cmd[:]  # Copy
    data = cmd[3:-4]  # Get only data params, exclude header and tail
    c = crc(data)  # Get data CRC
    crc_bytes = struct.pack('BB', c >> 8&0xFF, c & 0xFF)  # Update CRC field
    cmd_new = cmd[:-4] + crc_bytes + cmd[-2:]
    return cmd_new


def read_serial(ser, log, alive):
    print("Console open: {}".format(ser.isOpen()))
    while ser.isOpen():
        ans = ser.read(255)
        if len(ans) > 0:
            msg_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
            log.append("{}\n".format(ans))
            print('')
            print("[{}]".format(msg_time), ' '.join('{:02x}'.format(x) for x in ans))
            print("[{}]".format(msg_time), ans)
            print('')
        time.sleep(0.1)


commands = {
    "dummy": CmdDummyFrame,
    "alive": CmdAliveFrame,
    "trans": CmdTransferFrame,
    }


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("port", metavar='PORT', help="File with access list")
    #parser.add_argument("data", metavar='DATA', help="Task file")
    parser.add_argument("-d", "--delay", default=1, type=float, help="Delay between messages")
    parser.add_argument("-n", "--nmax", default=60, type=int, help="Number of messages")

    args = parser.parse_args()

    ser=serial.Serial(port=args.port,
        baudrate=115200,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
        )


    #print("Sending setup commands")
    ##cmds = [b'b 1\r\n', b'a 3\r\n', b'z\r\n']
    #cmds = []
    #for cmd in cmds:
        #print(cmd)
        #ser.write(cmd)
        #ans = ser.read(11)
        #print(' ', ans)

    log = []
    alive = True
    thr = threading.Thread(target=read_serial, args=(ser, log, alive), daemon=True)
    thr.start()

    print("Sending data...")
    #user_data = args.data if len(args.data) == 4 else args.data[:4]
    try:
        for i in range(args.nmax):
            data = input("Press enter to send commad >>").split(" ")
            ts = time.time()
            cmd = data[0]
            cmd = commands.get(cmd, commands["dummy"])()
            params = data[1:] if len(data) > 1 else None
            if params:
                params = [int(param) for param in params]
                cmd.set_params(*params)
            log.append("{}, {}, {}\n".format(datetime.datetime.now(), i, data))
            msg_time = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
            print("[{}]".format(msg_time), str(cmd))
            ser.write(cmd.to_bytes())
            # ans = ser.read(256)
            # log.append("{}\n".format(ans))
            # print(log[-1])
            delay = args.delay - (time.time() - ts)
            time.sleep(delay if delay > 0 else 0)
    except TypeError as e:
        print(e)
    except KeyboardInterrupt as e:
        print(e)
    except Exception as e:
        print(e)
    finally:
        print("Closing!")
        ser.close()
        with open("{}.txt".format(time.time()), "w") as logfile:
            logfile.writelines(log)

